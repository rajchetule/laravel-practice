@extends('layouts.index')
 
@section('content')
    <div id="content">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Product list (relational)</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('item.create') }}"> Create New Product</a>
                </div>
            </div>
        </div>
    
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
    
        <table class="table table-bordered">
            <tr>
                
                <th width="80px">No</th>
                <th>Product Name</th>
                <th>Product Details</th>
                <th>Product Category</th>
                <th>Action</th>
            </tr>        
            @foreach($items as $key => $item)
                <tr>
                    
                    <td>{{ ++$key }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->detail }}</td>
                    <td>{{ $item->category->name }}</td>
                    <td>
                        @if($item->status == 1)
                        <button class="btn btn-primary statusBtn" data-status="{{$item->status}} " data-sub-status="DEACTIVE" value="{{$item->id}}">Active</button>
                      @else
                        <button class="btn btn-warning statusBtn" data-status="{{$item->status}} " data-sub-status="ACTIVE" value="{{$item->id}}">Deactive</button>
                      @endif
                    </td>
                    
                    
                </tr>
            @endforeach
        
        </table>
    </div>
</div>
<script src="{{ asset('assets/node_modules/jquery/jquery.min.js') }}">
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</script>


<script >   

    $(document).ready(function(){
    
      $('.statusBtn').click(function(){
          var url = "/item/changeItemStatus";
          var id= $(this).val();
          var status = $(this).data('status');
          var sub_status = $(this).data('sub-status');
          var msg = (status== 0)? 'Activate' : 'Deactivate';
          if(confirm("Are you sure to "+ msg))
          {
              $.ajax({
                  type: 'get',
                  url: url,
                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  data: {"id":id,"status":status,"sub_status":sub_status},
                  success: function(data) {
                      console.log(data);
                      if(data.status){
                          location.reload();
                      }
                  },
                  error: { }
              });
          }
      });
       });
    </script>

</body>


      
@endsection