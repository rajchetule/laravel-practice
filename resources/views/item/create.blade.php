@extends('emp.layout')
  
@section('content')
<div class="row py-5">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Item</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href=""> Back</a>
        </div>
    </div>
</div>
   
   
<form action="{{ route('item.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
     <div class="row ">
        <div class="col-xs-12 col-sm-12 col-md-7">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" id="name" class="form-control"  maxlength="15"  placeholder="Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7">
            <div class="form-group">
                <strong>Detail:</strong>
                <input class="form-control" id="detail" name="detail"  placeholder="detail" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7">
            <div class="form-group">
                <label for="category">Category</label>
                <select class="form-control" id="category" name="categorie_id">
                  @foreach ($categories as $category)
                    <option  value="{{ $category->id }}">{{ $category->name }}</option>                  
                  @endforeach
                </select>
              </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-7 text-left">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        
    </div>
   
</form>


@endsection