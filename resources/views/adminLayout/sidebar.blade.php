<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> <a class="waves-effect waves-dark" href="{{ asset('html/index.html') }}" aria-expanded="false"><i
                            class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ asset('html/pages-profile.html') }}" aria-expanded="false"><i
                            class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ asset('html/table-basic.html') }}" aria-expanded="false"><i
                            class="fa fa-table"></i><span class="hide-menu">Tables</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ asset('html/icon-fontawesome.html') }}" aria-expanded="false"><i
                            class="fa fa-smile-o"></i><span class="hide-menu">Icons</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ asset('html/map-google.html') }}" aria-expanded="false"><i
                            class="fa fa-globe"></i><span class="hide-menu">Map</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ asset('html/pages-blank.html') }}" aria-expanded="false"><i
                            class="fa fa-bookmark-o"></i><span class="hide-menu">Blank</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ asset('html/pages-error-404.html') }}" aria-expanded="false"><i
                            class="fa fa-question-circle"></i><span class="hide-menu">404</span></a>
                </li>
            </ul>
            {{-- <div class="text-center mt-4">
                <a href="https://www.wrappixel.com/templates/adminwrap/"
                    class="btn waves-effect waves-light btn-info hidden-md-down text-white"> Upgrade to Pro</a>
            </div> --}}
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>