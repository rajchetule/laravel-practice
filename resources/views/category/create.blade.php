@extends('layouts.index')
  
@section('content')
<div class="row py-5">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Category</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('category.index') }}"> Back</a>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> This Category is already exist<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
   
<form action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
     <div class="row ">
        <div class="col-xs-12 col-sm-12 col-md-7">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" id="name" class="form-control"  maxlength="20"  placeholder="Name">
            </div>
        </div>  
        <div class="col-xs-12 col-sm-12 col-md-7 text-left">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        
    </div>
   
</form>


@endsection