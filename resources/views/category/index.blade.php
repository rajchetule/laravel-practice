@extends('layouts.index')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Category list</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('category.create') }}"> Create New Category</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            
            <th width="80px">No</th>
            <th>Product Name</th>
            <th width="270px">Action</th>
        </tr>        
        @foreach($category as $key => $cat)
            <tr>
                
                <td>{{ ++$key }}</td>
                <td>{{ $cat->name }}</td>               
                {{-- <td>{{ $category->name }}</td> --}}
                <td>
                    <form action="{{ route('category.destroy',$cat->id) }}" method="POST">
       
                        {{-- <a class="btn btn-info" href="{{ route('emp.show',$cat->id) }}">View</a> --}}
        
                        <a class="btn btn-primary" href="{{ route('category.edit',$cat->id) }}">Edit</a>
       
                        @csrf
                        {{-- @method('DELETE') --}}
          
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
                
            </tr>
        @endforeach
       
    </table>
</div> 


</body>


      
@endsection