@extends('emp.layout')
  
@section('content')
<div class="row py-5">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Employee</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('emp.index') }}"> Back</a>
        </div>
    </div>
</div>
   
   
<form action="{{ route('emp.store') }}" method="POST" id="emp-form">
    @csrf
  
     <div class="row ">
        <div class="col-xs-12 col-sm-12 col-md-7">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" id="name" class="form-control" minlength="7" maxlength="15"  placeholder="Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7">
            <div class="form-group">
                <strong>Post:</strong>
                <input class="form-control" id="post" name="post" minlength="3" maxlength="10" placeholder="Post" />
            </div>
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
        
    </div>
   
</form>

<script>
   $(document).ready(function() {
        $("#emp-form").validate({
        rules: {
        name : {
        required: true,
        minlength: 7
        maxlength: 15
        },
        post : {
        required: true,
        minlength: 3
        maxlength: 10
        },
        }
        });
    });
</script>
@endsection