@extends('emp.layout')
 
@section('content')
    <div class="row pt-5">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 5.8 CRUD Operation</h2>
            </div>
            <div class="pull-right pb-4">
                <a class="btn btn-success" href="{{ route('emp.create') }}"> Create New Employee</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Post</th>
            <th width="270px">Action</th>
        </tr>
        @foreach ($emp as $emp)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $emp->name }}</td>
            <td>{{ $emp->post }}</td>
            <td>
                <form action="{{ route('emp.destroy',$emp->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('emp.show',$emp->id) }}">View</a>
    
                    <a class="btn btn-primary" href="{{ route('emp.edit',$emp->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    
      
@endsection