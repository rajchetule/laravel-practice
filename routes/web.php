<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/registerMail/{data}','Auth\RegisterController@index');
// Route::get('/register/{data}','Auth\RegisterController@mailsending');

Route::get('/admin/edit', 'EditProfileController@index')->name('admin-edit');
Route::post('/admin/update/', 'EditProfileController@update')->name('admin-update');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/user/logout', 'Auth\LoginController@userLogout')->name('user.logout');
// --------------------------------------multi auth middleware -----------------------
Route::group(['prefix' => 'admin'], function() {
	Route::group(['middleware' => 'admin.guest'], function(){
		Route::view('/login','admin.login')->name('admin.login');
		Route::post('/login','AdminController@authenticate')->name('admin.auth');
	});
	
	Route::group(['middleware' => 'admin.auth'], function(){
		Route::get('/dashboard','DashboardController@dashboard')->name('admin.dashboard');
        Route::get('/logout', 'AdminController@logout')->name('admin.logout');
	});
});

// ----------------------------------------- crud operation with ajax & simple ------------------------
Route::resource('ajaxemp','EmpAjaxController');
Route::resource('emp','EmpController');

//-------------------------- mail functionality---------------------- 

Route::get('mail', 'HomeController@mail');

Route::get('mailsend', function () {
   
    $details = [
        'title' => 'Mail from system',
        'body' => 'This is for testing email using smtp'
    ];
   
    \Mail::to('rajchetule@gmail.com')->send(new \App\Mail\MyTestMail($details));
   
    dd("Email is Sent.");
});

Route::get('send-mail','MailSend@mailsend');

Route::get('contact','ContactController@index');
Route::post('contact-submit','ContactController@mailsending');

// -------------- payment ===================================================
Route::get('razorpay-payment', 'RazorpayPaymentController@index');
Route::post('razorpay-payment', 'RazorpayPaymentController@store')->name('razorpay.payment.store');

// ============================ multiple delete functionality -=======================
Route::get('/myproducts', 'ProductController@index')->name('products.index');
Route::get('/myproducts/create', 'ProductController@create')->name('products.create');
Route::post('/myproducts/store', 'ProductController@store')->name('products.store');
Route::any('/myproducts/{id}', 'ProductController@destroy')->name('products.delete');
Route::any('myproductsDeleteAll', 'ProductController@deleteAll')->name('products.allDelete');

// ---------relation--------
// Route::get('addItem/{id}', 'ItemController@addItem');
Route::get('item/index/', 'ItemController@index')->name('item.index');
Route::get('item/create', 'ItemController@create')->name('item.create');
Route::post('item/store', 'ItemController@store')->name('item.store');
Route::get('item/changeItemStatus', 'ItemController@changeItemStatus')->name('item.changeItemStatus');

//-----------------item or product category---------------
Route::get('category/index', 'ProductCategoryController@productCategory')->name('category.index');
Route::get('category/create', 'ProductCategoryController@create')->name('category.create');
Route::post('category/store', 'ProductCategoryController@store')->name('category.store');
Route::get('category/edit/{id}', 'ProductCategoryController@edit')->name('category.edit');
Route::post('category/update/{id}', 'ProductCategoryController@update')->name('category.update');
Route::post('category/destroy/{id}', 'ProductCategoryController@destroy')->name('category.destroy');
