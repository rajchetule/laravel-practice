<?php

namespace App\Traits;
use Illuminate\Http\Request;
use app\User;
use Illuminate\Support\Facades\Mail;
use \App\Mail\SendMail;

trait UserCheck {

    public function UserMail($details) {
        
        // dd($request);
        
        // $user = User::find(1)->toArray();
        // Mail::send('emails.mailEvent', function($message) {
        //     $message->to('rajchetule@gmail.com');
        //     $message->subject('Testing mail');
        // });
        $details = [
            'title' => 'Title: Mail from My system',
            'body' => 'Body: This is for testing email using smtp'
        ];

        Mail::to('rajchetule@gmail.com')->send(new SendMail($details));
        // return view('emails.thanks');
    }
}