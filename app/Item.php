<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Item extends Model
{
    
    protected $table = 'items';

    protected $fillable = ['name','detail','status','categorie_id'];

    public function category(){

        return $this->belongsTo('App\Category', 'categorie_id', 'id');

    }

}
