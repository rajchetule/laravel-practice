<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class AdminAuthenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('admin.login');
        }
    }

    protected function authenticate($request, array $guards)
    {
            if ($this->auth->guard('admin')->check()){
                // dd('fail');
                return $this->auth->shouldUse('admin');
            }
            // return redirect()->route('admin.login');
            $this->unauthenticated($request,['admin']);

        
    }
}
