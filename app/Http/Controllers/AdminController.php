<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Providers\RouteServiceProvider;

class AdminController extends Controller
{
    public function authenticate(Request $request)
    {
        // dd('hi');
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password],$request->get('remember'))){
            return redirect()->route('admin.dashboard');

        }else{
            session()->flash('error', 'admin credential only');
            return back()->withInput($request->only('email'));
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        // dd('out');
        return redirect()->route('admin.login');
    }
}
