<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emp;
use DataTables;

class EmpAjaxController extends Controller
{
    
    public function index(Request $request)
    {
   
        if ($request->ajax()) {
            $data = Emp::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editEmp">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteEmp">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('empAjax');
    }

    public function create()
    {
        //
    }

     public function store(Request $request)
    {
        Emp::updateOrCreate(['id' => $request->emp_id],
                ['name' => $request->name, 'post' => $request->post]);        
   
        return response()->json(['success'=>'Employee saved successfully.']);
    }
    
    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $emp = Emp::find($id);
        return response()->json($emp);
    }


    public function update(Request $request, $id)
    {
        //
    }

     public function destroy($id)
    {
        Emp::find($id)->delete();
     
        return response()->json(['success'=>'Employee deleted successfully.']);
    }
}
