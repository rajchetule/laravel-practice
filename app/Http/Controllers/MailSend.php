<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Mail\SendMail;
use app\User;

class MailSend extends Controller
{
    //
    public function mailsend()
    {
        $details = [
            'title' => 'Title: Mail from My system',
            'body' => 'Body: This is for testing email using smtp'
        ];

        \Mail::to('rajchetule@gmail.com')->send(new SendMail($details));
        return view('emails.thanks');
    }
}
