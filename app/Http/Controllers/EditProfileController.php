<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class EditProfileController extends Controller
{
    public function index(Request $request, User $user)
    {
        $user = Auth::user();
        // dd(Auth::user()->id);
        return view('editProfile', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required|unique:users',
            // 'email' => 'required|email|max:20|unique:users',
        ]);
        $input = $request->only('name');
        // dd($input);

        $user = Auth::user();
        $user->update($input);
  
        return redirect()->route('home')
        ->with('alert','User updated successfully');
    }
}
