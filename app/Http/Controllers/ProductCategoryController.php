<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Item;
use DB;
use Illuminate\Support\Facades\Input;

class ProductCategoryController extends Controller
{
    public function productCategory()
    {
        $category = DB::table("categories")->get();
  
        return view('category.index',compact('category'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        if ($request->isMethod('POST')) {
            $request->validate([
                'name' => ['required','string', 'unique:categories']
            ]);
            // dd($request->name);
            $data = $request->all();
            // dd($data);
            // $category = Category::create($data);
            // $category->created_by = Auth::guard('admin')->user()->id;
            // $category->updated_by = Auth::guard('admin')->user()->id;
            $category = Category::where('name','=', Input::get('name'))->first();
            // dd($category);
            if ($category == null) {
                // dd('ok');
                Category::create($data);
                return redirect()->route('category.index')
                ->with('success','Category created successfully.');
            }           
        }
        

    }

    public function edit(Category $category, $id)
    {
        // $category = DB::table("categories")->first();
        $category = Category::find($id);
        // dd($category);
        return view('category.edit',compact('category'));
    }
    
    public function update(Request $request, Category $category, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category = Category::find($id);
        $category->name = $request->input('name');
        $category->update();
  
        return redirect()->route('category.index')
        ->with('success','Employee updated successfully');
    }

    
    public function destroy(Category $category, $id)
    {
        // $category = Category::where('id', $id)->firstorfail()->delete();
        $items_all = DB::table("items")->get();
        $item_cat = Item::pluck('categorie_id');
       
        // dd($item_cat);
        $category = Category::find($id); 
        $item = $category->name;
        // $item = $category->name;
        // dd($item);
        if (!$item_cat == $id) {
            $category = Category::where('id', $id)->firstorfail()->delete();
            return redirect()->route('category.index')
            ->with('success','Category deleted successfully');
        }
        
        // $category->delete();
  
        
    }
}
