<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emp;
use Auth;

class EmpController extends Controller
{
    
    public function index()
    {
        
        // dd(Auth::user()->id);
        $emp = Emp::latest()->paginate(5);
  
        return view('emp.index',compact('emp')) ->with('i', (request()->input('page', 1) - 1) * 5);;
    }

    public function create()
    {
        //
        return view('emp.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'post' => 'required',
        ]);
  
        Emp::create($request->all());
   
        return redirect()->route('emp.index')
        ->with('success','Employee created successfully.');
    }

    
    public function show(Emp $emp)
    {
        return view('emp.show',compact('emp'));
    }

    
    public function edit(Emp $emp)
    {
        return view('emp.edit',compact('emp'));
    }
    
    public function update(Request $request, Emp $emp)
    {
        $request->validate([
            'name' => 'required',
            'post' => 'required',
        ]);
  
        $emp->update($request->all());
  
        return redirect()->route('emp.index')
        ->with('success','Employee updated successfully');
    }

    
    public function destroy(Emp $emp)
    {
        
        $emp->delete();
  
        return redirect()->route('emp.index')
        ->with('success','Employee deleted successfully');
    }
}
