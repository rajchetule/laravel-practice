<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\User;
use Illuminate\Support\Facades\Mail;
use App\Traits\UserCheck;

class HomeController extends Controller
{
    use UserCheck;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function mail(Request $request, User $user)
    {
        $this->middleware('auth');
        $user = User::find(1)->toArray();
        if ($user != "") {
            // dd($request);
            $this->UserMail($user);
            return view('emails.thanks');;
        }
        // Mail::send('emails.mailEvent', $user, function($message) use ($user) {
        //     $message->to('rajchetule@gmail.com');
        //     $message->subject('Testing mail');
        // });
        // dd('Mail Send Successfully');
    }
}
