<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;

class ProductController extends Controller
{
    //
    public function __construct()
    {
        $products = Product::latest()->paginate(5);
  
        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function index()
    {
        // dd('index');
        $products = DB::table("products")->get();
        return view('products.index',compact('products'));
    }

    public function create()
    {
        // dd('creaate');
        return view('products.create');
    }

    public function store(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);   
        // dd($request->all());
        Product::create($request->all());
   
        return redirect()->route('products.index')
                        ->with('success','Product created successfully.');
    }

    public function destroy($id)
    {
        // dd($id);
    	DB::table("products")->delete($id);
    	return response()->json(['success'=>"Product Deleted successfully.", 'tr'=>'tr_'.$id]);
    }

     public function deleteAll(Request $request, Product $product)
    {
        $ids = $request->ids;
        dd($ids);
        DB::table("products")->whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
}
