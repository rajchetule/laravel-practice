<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Item;
use DB;
use Auth;

class ItemController extends Controller
{
    
    // public function __construct(Request $request)
    // {
    //     if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password],$request->get('remember'))){
    //         return redirect()->route('admin.dashboard');
    //     }else{
    //         session()->flash('error', 'admin credential only');
    //         return back()->withInput($request->only('email'));
    //     }
    // }

    public function index(Request $request)
    {
        $items = Item::with('category')->get();
        // dd($items);
        return view('item.index',compact('items'));

    }

    public function create()
    {
        $categories = DB::table("categories")->get();
        // dd($category);
        return view('item.create', compact('categories'));
    }

    public function store(Request $request)
    {
    
        $categories = DB::table("categories")->get();
        $data = $request->all();
        // dd($data);
        Item::create($data);

        return redirect()->route('item.index', compact('categories') )
        ->with('success','Item created successfully.');
        // dd($data);

    }

    public function changeItemStatus(Request $request)
    {
        if ($request->ajax()) {
            // dd($request->all());
            $id = $request->input('id');
            $status = $request->input('status');
            $sub_status = $request->input('sub_status');
            if($sub_status == 'ACTIVE')
            {
                $subs_type = 'free';
            }
            else{
                $subs_type = 'null';
            }
            if ($status == 0) {
                $status = 1;
            } else {
                $status = 0;
            }
            $check = DB::table('items')->where('id', $id)->update([
                'status' => $status,
               
                
                ]);
            if ($check) {
                return ['status' => true];
            } else {
                return ['status' => false];
            }
        }
    }
}
