<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Admin;
use Validator;

class AdminLoginController extends Controller
{
    //

    public function index()
    {
        // dd(Auth::user()->id);
        return view('adminLogin');
    }

    public function adminHome()
    {
        return view('adminHome');
    }

    public function login(Request $request)
    {
        
    	Validator::make($request->all(), [
    		'email' => 'required|email',
    		'password' => 'required|min:6',
    	])->validate();

             if (Auth::guard('admin')->attempt([
                'email' => $request->email,
                'password' => $request->password
            ], 
                $request->remember)) {
                    // dd('hii');
                return redirect()->intended(route('admin-home'));
            } else {
                // dd('hello');
                return redirect()->route('admin');
            }
    }
}
