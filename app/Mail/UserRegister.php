<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegister extends Mailable
{
    use Queueable, SerializesModels;
    public $user_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_data)
    {
        $this->user_data = $user_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');

        $from_name = "Laravel Project User";
        $from_email = "rajguideline@gmail.com";
        $subject = "Login successful In Laravel.com";
        // dd('gfgb');
        return $this->from($from_email, $from_name)
            ->view('emails.contact')
            ->subject($subject);
    }
}
