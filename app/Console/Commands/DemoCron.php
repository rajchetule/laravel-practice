<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DemoCron extends Command
{
    
    protected $signature = 'demo:cron';

    protected $description = 'Command description';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //
        \Log::info("Cron is working fine!");

        $this->info('Demo:Cron Cummand Run successfully!');
    }
}
